<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$hewan = new Animal("domba");
echo "Nama Hewan : ". $hewan->name . "<br>";
echo "Jumlah Kaki Hewan : ". $hewan->legs . "<br>";
echo "Berdarah dingin : ". $hewan->cold_blooded . "<br><br>";

$kodok = new frog("buduk");
echo "Nama Hewan : ". $kodok->name . "<br>";
echo "Jumlah Kaki Hewan : ". $kodok->legs . "<br>";
echo "Berdarah dingin : ". $kodok->cold_blooded . "<br>";
echo $kodok->jump() . "<br><br>";

$sungokong = new ape("kera sakti");
echo "Nama Hewan : ". $sungokong->name . "<br>";
echo "Jumlah Kaki Hewan : ". $sungokong->legs . "<br>";
echo "Berdarah dingin : ". $sungokong->cold_blooded . "<br>";
echo $sungokong->yell()

?>