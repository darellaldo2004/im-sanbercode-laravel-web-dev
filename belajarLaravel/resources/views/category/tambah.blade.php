@extends('layout.master')
@section('title')
    Halaman Tambah Category
    @endsection
    @section('content')

    <form method="POST" action="/category">
        @csrf
        <div class="form-group">
          <label >Category Name</label>
          <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
          <label>Category Description</label>
          <textarea name="description" class="form-control" cols="30" rows="10"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>

     @endsection