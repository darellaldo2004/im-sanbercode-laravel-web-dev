<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\BioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'dashboard']);
Route::get('/daftar', [BioController::class, 'daftar']);
Route::post('/kirim', [BioController::class, 'kirim']);

//testing tampilan template
Route::get('/master', function(){
    return view('layout.master');
});

//CRUD

//CREATE DATA
//route untuk mengarah ke form tambah kategori
Route::get('/category/create', [CategoriesController::class, 'create']);

//route untuk menyimpan data inputan ke DB table category
Route::post('/category', [CategoriesController::class, 'store']);

//READ DATA
//Route untuk menampilkan semua data yang ada di table kategori database
Route::get('/category',[CategoriesController::class, 'index']);
