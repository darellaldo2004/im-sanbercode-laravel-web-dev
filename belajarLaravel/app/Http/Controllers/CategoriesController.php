<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function create()
    {
        return view('category.tambah');
    }

    public function store(Request $request)
    {
        //validasi data
        $request->validate([
            'name' => 'required|min:5',
            'description' => 'required|min:5',
        ]);

        //masukkan data request ke table categories di database
        DB::table('categories')->insert([
            'name' => $request['name'],
            'description' => $request['description']
        ]);

        //kita lempar ke halaman /category
        return redirect('/category');
    }   

    public function index()
    {
        $categories = DB::table('categories')->get();
        return view('category.tampil', ['categories' => $categories]);
    }
}
